// Require dependencies
// const logger = require("morgan");
const express = require("express");
const axios = require("axios");
const parseUrl = require("./scrapper");

const URL = "https://iwastesomuchtime.com/";

// Create an Express application
const app = express();

// Configure the app port
const port = process.env.PORT || 3000;
app.set("port", port);

// Load middlewares
// app.use(logger("dev"));

app.get("/", function (req, res) {
	axios
		.get(URL)
		.then((response) => parseUrl(response.data))
		.then((list) => res.send(list))
		.catch((err) => console.log(err));
});
app.get("/:pageNo", function (req, res) {
	const { pageNo } = req.params;
	axios
		.get(URL, { params: { page: pageNo } })
		.then((response) => parseUrl(response.data))
		.then((list) => res.send(list))
		.catch((err) => console.log(err));
});

app.all("*", function (req, res) {
	res.sendStatus(404);
});

// Start the server and listen on the preconfigured port
app.listen(port);
