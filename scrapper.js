const cheerio = require("cheerio");

function parseUrl(html) {
	list = [];
	$ = cheerio.load(html);
	$("img").each((i, { attribs }) => {
		src = attribs.src;
		title = attribs.alt;
		if (src.includes("cdn")) list.push({ src, title });
	});
	return list;
}

module.exports = parseUrl;
